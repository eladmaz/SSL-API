package org.ssldev.api.services;

import java.util.HashMap;
import java.util.Map;

import org.ssldev.api.chunks.Adat;
import org.ssldev.api.messages.AdatConsumedMessage;
import org.ssldev.api.messages.TrackLoadedMessage;
import org.ssldev.api.messages.TrackUnloadedMessage;
import org.ssldev.api.messages.TrackUpdatedMessage;
import org.ssldev.core.messages.MessageIF;
import org.ssldev.core.mgmt.EventHub;
import org.ssldev.core.services.Service;
import org.ssldev.core.utils.Logger;
import org.ssldev.core.utils.SysInfo;
/**
 * models a set of decks and publishes out whenever Serato reports a track was
 * loaded/unloaded from a deck, or track update was received. 
 * <p>
 * In practice, Serato (2.1.0) publishes a track 'add' when a track first gets
 * loaded into an empty deck.  And a track 'remove' followed by a track 'add'
 * when a loaded track is replaced with another.  Track updates seem to 
 * happen at infrequent intervals.
 * <p>
 * It should be noted that the data that Serato reports (and the publishing)
 * may be different in different play modes (e.g. offline, connected)
 * 
 * Consumes: 
 * <ul>
 * <li>{@link AdatConsumedMessage}</li>
 * </ul>
 * Produces:
 * <ul>
 * <li>{@link TrackLoadedMessage}</li>
 * <li>{@link TrackUnloadedMessage}</li>
 * <li>{@link TrackUpdatedMessage}</li>
 * </ul>
 */
public class TrackPublisherService extends Service {
	private Map<Integer,Deck> decks = new HashMap<>();
	
	private static final Adat EMPTY = new Adat();
	
	/** ensure serial track processing */
	private final Object lock = new Object();

	public TrackPublisherService(EventHub hub) {
		super(hub);
	}
	
	public void notify(MessageIF msg) {
		if(!(msg instanceof AdatConsumedMessage)) return;
		
		synchronized (lock) {
			process(((AdatConsumedMessage)msg).adat);
		}
	}

	private void process(Adat adat) 
	{
		decks.computeIfAbsent(adat.getDeck(), Deck::new)
			 .onChange(adat);
	}

	private class Deck{
		private final int deck;
		private Adat loaded = EMPTY;
		
		public Deck(int num) {
			deck = num;
		}

		/**
		 * notification that an adat update was received for 
		 * this deck
		 * @param adat
		 */
		public void onChange(Adat adat) {
			/*
			 * 1. cue tack removed
			 * 2. cue track added
			 * 3. track playing add / playing update
			 */
			if(isRemoved(adat)) {
				// track has been removed
				onRemove(adat);
			}
			else if(isAdded(adat)) {
				// track has been added
				onAdd(adat);
			}
			else {
				// track was updated
				onUpdate(adat);
			}
		}

		private void onUpdate(Adat adat) {
			publishTrackUpdated(adat);
		}

		private void onAdd(Adat adat) {
			if(!loaded.getTitle().equals(adat.getTitle())) {
				loaded = adat;
				publishTrackLoaded(adat);
			}
			else {
				onUpdate(adat);
			}
		}

		private void onRemove(Adat adat) {
			loaded = EMPTY;
			publishTrackUnloaded(adat);
		}

		
		// currently serato sets the update time equal to the 'startTime', when a 
		// track is first added to a deck
		private boolean isAdded(Adat adat) {
			return adat.getStartTime() == adat.getUpdateTime();
		}

		// currently serato updates an Adat's 'totalPlayTime' field when it is removed
		// from a deck
		private boolean isRemoved(Adat adat) {
			return adat.getTotalPlayTime() > 0;
		}
		

		private void publishTrackLoaded(Adat adat) {
			Logger.info(this, toString() + "[Added] "+adat.getArtist() +" - " +adat.getTitle() + " ["+adat.getBpm()+"]");
			Logger.debug(this, adat.toString());
			hub.add(new TrackLoadedMessage(adat.getStartTime(), deck, adat));
		}

		private void publishTrackUpdated(Adat adat) {
			Logger.info(this, toString() + "[Update] "+adat.getArtist() +" - " +adat.getTitle() + " ["+adat.getBpm()+"] : "
					+ " start: "+SysInfo.getDate(adat.getStartTime() * 1000)
					+", lastUpdate: " + SysInfo.getDate(adat.getUpdateTime() * 1000));
			Logger.debug(this, adat.toString());
			hub.add(new TrackUpdatedMessage(adat.getUpdateTime(), adat, deck));
		}
		
		private void publishTrackUnloaded(Adat adat) {
			Logger.info(this, toString() + "[Remove] "+adat.getArtist() +" - " +adat.getTitle() + " ["+adat.getBpm()+"] : "
					+" total playTime: " + SysInfo.getTimeInMinutesSeconds(adat.getTotalPlayTime() * 1000));
			Logger.debug(this, adat.toString());
			hub.add(new TrackUnloadedMessage(adat.getEndTime(), deck, adat));
		}
		
		@Override
		public String toString() {
			return "[DECK "+deck+"]";
		}
	}

	@Override
	public void shutdown() {
		super.shutdown();
		decks.clear();
	}
}
